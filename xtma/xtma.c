/******************************************************************************
 * xtma.c
 *****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include <X11/Xlib.h>
#include <X11/keysym.h>


int main(int argc, char ** argv)
{
    Display *dpy;
    int      screen;
    int      dispw;
    int      disph;
    Window   win;
    int run = 1;
    XEvent xev;
    Atom wm_state, fs;
    Pixmap bm_no;
    Colormap cmap;
    Cursor no_ptr;
    XColor black, dummy;
    static char bm_no_data[] = {0, 0, 0, 0, 0, 0, 0, 0};

    close(0); close(1); close(2);

    while(1)
    {
        dpy = XOpenDisplay("");
        if(NULL != dpy) break;
        sleep(1);
    }

    screen = DefaultScreen(dpy);
    dispw = XDisplayWidth(dpy, screen);
    disph = XDisplayHeight(dpy, screen);

    win = XCreateSimpleWindow(dpy, RootWindow(dpy, screen), 0, 0, 1, 1, 0, WhitePixel(dpy, screen), BlackPixel(dpy, screen));

    /* disable the cursor: http://www.linuxforums.org/forum/linux-programming-scripting/59012-xlib-hide-mouse-pointer.html */
    cmap = DefaultColormap(dpy, screen);
    XAllocNamedColor(dpy, cmap, "black", &black, &dummy);
    bm_no = XCreateBitmapFromData(dpy, win, bm_no_data, 8, 8);
    no_ptr = XCreatePixmapCursor(dpy, bm_no, bm_no, &black, &black, 0, 0);
    XDefineCursor(dpy, win, no_ptr);
	
    /* name */
    XStoreName(dpy, win, "xtma");

    /* show the window */
    XMapRaised(dpy, win);

    /* listen for mouse and kbd */
    XSelectInput(dpy, win, KeyPressMask | ButtonPressMask); 

    /* fullscreen */
    wm_state = XInternAtom(dpy, "_NET_WM_STATE", True);
    fs = XInternAtom(dpy, "_NET_WM_STATE_FULLSCREEN", True);
    memset(&xev, 0, sizeof(xev));
    xev.type = ClientMessage;
    xev.xclient.serial = 0;
    xev.xclient.send_event=True; 
    xev.xclient.window = win;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = 1;
    xev.xclient.data.l[1] = fs;
    xev.xclient.data.l[2] = 0;
    XSendEvent(dpy, DefaultRootWindow(dpy), False, SubstructureRedirectMask | SubstructureNotifyMask, &xev);
    XFlush(dpy);


    while(run)
    {
        XNextEvent(dpy, &xev);
        if(ButtonPress == xev.type || (KeyPress == xev.type && XK_Escape == (XLookupKeysym((XKeyEvent *)&xev, 0)))) run = 0;
    }


    XCloseDisplay(dpy);

    return 0;
} /* main() */

