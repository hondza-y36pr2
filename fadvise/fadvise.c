/******************************************************************************
 * fadvise.c
 *****************************************************************************/

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdarg.h>
#include <dlfcn.h>

#include "fadvise.h"

static real_close_t real_close;
static real_write_t real_write;
static real_read_t real_read;

static real_fclose_t real_fclose;
static real_fwrite_t real_fwrite;
static real_fread_t real_fread;
static real_fwrite_t real_fwrite_unlocked;
static real_fread_t real_fread_unlocked;

static pid_t mypid;

static int fadvise_init_done=0;
static int fadvise_debug=0;
static int fadvise_advice=POSIX_FADV_DONTNEED;

void do_log(char *str, ...)
{
    va_list arglist;

    va_start(arglist, str);
    vfprintf(stderr, str, arglist);
    fflush(stderr);
    va_end(arglist);
} /* do_log() */



static void lib_init(void)
{
    char *temp;

    if(fadvise_init_done) return;
    fadvise_init_done=1;

    *(void **) (&real_close) = dlsym(RTLD_NEXT, "close");
    if(!real_close)
    {   
        fprintf(stderr, "(fadvise) Cannot load symbol 'close' %s\n", dlerror());
        _exit(1);
    }
    *(void **) (&real_write) = dlsym(RTLD_NEXT, "write");
    if(!real_write)
    {   
        fprintf(stderr, "(fadvise) Cannot load symbol 'write' %s\n", dlerror());
        _exit(1);
    }
    *(void **) (&real_read) = dlsym(RTLD_NEXT, "read");
    if(!real_read)
    {   
        fprintf(stderr, "(fadvise) Cannot load symbol 'read' %s\n", dlerror());
        _exit(1);
    }
    *(void **) (&real_fclose) = dlsym(RTLD_NEXT, "fclose");
    if(!real_fclose)
    {   
        fprintf(stderr, "(fadvise) Cannot load symbol 'fclose' %s\n", dlerror());
        _exit(1);
    }
    *(void **) (&real_fwrite) = dlsym(RTLD_NEXT, "fwrite");
    if(!real_fwrite)
    {   
        fprintf(stderr, "(fadvise) Cannot load symbol 'fwrite' %s\n", dlerror());
        _exit(1);
    }
    *(void **) (&real_fread) = dlsym(RTLD_NEXT, "fread");
    if(!real_fread)
    {   
        fprintf(stderr, "(fadvise) Cannot load symbol 'fread' %s\n", dlerror());
        _exit(1);
    }
    *(void **) (&real_fwrite_unlocked) = dlsym(RTLD_NEXT, "fwrite_unlocked");
    if(!real_fwrite_unlocked)
    {   
        fprintf(stderr, "(fadvise) Cannot load symbol 'fwrite_unlocked' %s\n", dlerror());
        _exit(1);
    }
    *(void **) (&real_fread_unlocked) = dlsym(RTLD_NEXT, "fread_unlocked");
    if(!real_fread_unlocked)
    {   
        fprintf(stderr, "(fadvise) Cannot load symbol 'fread_unlocked' %s\n", dlerror());
        _exit(1);
    }
    
    temp = getenv("FADVISE_ADVICE"); if(!temp) temp = getenv("FADV");
    if(temp)
    {
        if(strcasestr(temp, "NORM")) fadvise_advice = POSIX_FADV_NORMAL;
        else if(strcasestr(temp, "SEQ")) fadvise_advice = POSIX_FADV_SEQUENTIAL;
        else if(strcasestr(temp, "RAND")) fadvise_advice = POSIX_FADV_RANDOM;
        else if(strcasestr(temp, "NORE")) fadvise_advice = POSIX_FADV_NOREUSE;
        else if(strcasestr(temp, "WILL")) fadvise_advice = POSIX_FADV_WILLNEED;
        else if(strcasestr(temp, "DONT")) fadvise_advice = POSIX_FADV_DONTNEED;
    }

#ifndef NDEBUG
    temp = getenv("FADVISE_DEBUG"); if(!temp) temp = getenv("FADD");
    if(temp) fadvise_debug = strtoul(temp, NULL, 10);
#endif /* NDEBUG */


} /* lib_init() */



ssize_t write(int fd, const void *buf, size_t count)
{
    ssize_t ret;
    off_t pos;

    DEBUG("write(%d, ,%u)\n", fd, count);
    if(!fadvise_init_done) lib_init();

    ret = real_write(fd, buf, count);

    if(ret > 0)
    {
        pos = lseek(fd, 0, SEEK_CUR);
        if((off_t)-1 == pos) pos = 0;
        posix_fadvise(fd, 0, pos, fadvise_advice);
    }

    return ret;
}



size_t fwrite(const void *ptr, size_t size, size_t nmemb, FILE *stream)
{
    size_t ret;
    off_t pos;

    DEBUG("fwrite(, %u, %u, %d)\n", size, nmemb, stream ? fileno(stream) : -1);
    if(!fadvise_init_done) lib_init();

    ret = real_fwrite(ptr, size, nmemb, stream);

    if(ret > 0)
    {
        pos = (long) ftell(stream);
        if(-1 == pos) pos = 0;
        posix_fadvise(fileno(stream), 0, pos, fadvise_advice);
    }

    return ret;
}



size_t fwrite_unlocked(const void *ptr, size_t size, size_t nmemb, FILE *stream)
{
    size_t ret;
    off_t pos;

    DEBUG("fwrite_unlocked(, %u, %u, %d)\n", size, nmemb, stream ? fileno(stream) : -1);
    if(!fadvise_init_done) lib_init();

    ret = real_fwrite_unlocked(ptr, size, nmemb, stream);

    if(ret > 0)
    {
        pos = (long) ftell(stream);
        if(-1 == pos) pos = 0;
        posix_fadvise(fileno(stream), 0, pos, fadvise_advice);
    }

    return ret;
}



ssize_t read(int fd, void *buf, size_t count)
{
    ssize_t ret;
    off_t pos;

    DEBUG("read(%d, ,%u)\n", fd, count);
    if(!fadvise_init_done) lib_init();

    ret = real_read(fd, buf, count);

    if(ret > 0)
    {
        pos = lseek(fd, 0, SEEK_CUR);
        if((off_t)-1 == pos) pos = 0;
        posix_fadvise(fd, 0, pos, fadvise_advice);
    }

    return ret;
}



size_t fread(void *ptr, size_t size, size_t nmemb, FILE *stream)
{
    size_t ret;
    off_t pos;

    DEBUG("fread(, %u, %u, %d)\n", size, nmemb, stream ? fileno(stream) : -1);
    if(!fadvise_init_done) lib_init();

    ret = real_fread(ptr, size, nmemb, stream);

    if(ret > 0)
    {
        pos = (long) ftell(stream);
        if(-1 == pos) pos = 0;
        posix_fadvise(fileno(stream), 0, pos, fadvise_advice);
    }

    return ret;
}



size_t fread_unlocked(void *ptr, size_t size, size_t nmemb, FILE *stream)
{
    size_t ret;
    off_t pos;

    DEBUG("fread_unlocked(, %u, %u, %d)\n", size, nmemb, stream ? fileno(stream) : -1);
    if(!fadvise_init_done) lib_init();

    ret = real_fread_unlocked(ptr, size, nmemb, stream);

    if(ret > 0)
    {
        pos = (long) ftell(stream);
        if(-1 == pos) pos = 0;
        posix_fadvise(fileno(stream), 0, pos, fadvise_advice);
    }

    return ret;
}




int close(int fd)
{
    off_t pos;

    DEBUG("close(%d)\n", fd);
    if(!fadvise_init_done) lib_init();

    pos = lseek(fd, 0, SEEK_CUR);
    if((off_t)-1 == pos) pos = 0;
    posix_fadvise(fd, 0, pos, fadvise_advice);
    return real_close(fd);
}



int fclose(FILE *fp)
{
    off_t pos;

    DEBUG("fclose(%d)\n", fp ? fileno(fp) : -1);
    if(!fadvise_init_done) lib_init();

    pos = (long) ftell(fp);
    if(-1 == pos) pos = 0;
    posix_fadvise(fileno(fp), 0, pos, fadvise_advice);
    return real_fclose(fp);
}


