/******************************************************************************
 * fadvise.h
 *****************************************************************************/

#ifndef _FADVISE_H_
#define _FADVISE_H_


#ifndef NDEBUG
#define DEBUG(p...) do { if(fadvise_debug) do_log(p); } while(0)
#else
#define DEBUG(p...)
#endif /* NDEBUG */

#undef fwrite_unlocked
#undef fread_unlocked


typedef int (*real_close_t) (int);

typedef ssize_t (*real_write_t) (int, const void *, size_t);

typedef ssize_t (*real_read_t) (int, void *, size_t);

typedef size_t (*real_fwrite_t) (const void *, size_t, size_t, FILE *);

typedef size_t (*real_fread_t) (void *, size_t, size_t, FILE *);

typedef int (*real_fclose_t) (FILE *);

static void lib_init(void) __attribute__((constructor));

/* static void lib_deinit(void) __attribute__((destructor)); */

#endif /* _FADVISE_H_ */


