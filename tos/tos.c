/******************************************************************************
 * tos.c
 *****************************************************************************/

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <dlfcn.h>

#include "tos.h"


real_socket_t real_socket;
real_setsockopt_t real_setsockopt;
int default_tos = 0x18;



void lib_init(void)
{
    char *temp;

    *(void **) (&real_socket) = dlsym(RTLD_NEXT, "socket");
    if(!real_socket)
    {   
        fprintf(stderr, "Cannot load symbol 'socket' %s\n", dlerror());
        _exit(1);
    }

    *(void **) (&real_setsockopt) = dlsym(RTLD_NEXT, "setsockopt");
    if(!real_setsockopt)
    {   
        fprintf(stderr, "Cannot load symbol 'setsockopt' %s\n", dlerror());
        _exit(1);
    }

    temp = getenv("TOS");
    if(temp) default_tos = (int) strtoul(temp, NULL, 10);
}



int setsockopt(int s, int level, int optname, const void *optval, socklen_t optlen)
{
    if(level == SOL_IP && optname == IP_TOS) return 0;   
    return real_setsockopt(s, level, optname, optval, optlen);
}



int socket(int domain, int type, int protocol)
{
    int err2, tos = default_tos;
    int err = real_socket(domain, type, protocol);

    if(domain != PF_INET || err == -1) return err;

    err2 = real_setsockopt(err, SOL_IP, IP_TOS, (void *) &tos, sizeof (int));
    if(err2 == -1) { perror("setsockopt(... IP_TOS ...)"); exit(1); }

    return err;
}


