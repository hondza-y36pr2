/******************************************************************************
 * tos.h
 *****************************************************************************/

#ifndef _TOS_H_
#define _TOS_H_

typedef int (*real_setsockopt_t) (int, int, int, const void *, socklen_t );

typedef int (*real_socket_t) (int, int, int);

void lib_init(void) __attribute__((constructor));


#endif /* _TOS_H_ */


