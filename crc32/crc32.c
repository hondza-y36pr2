/*
 * This is a tiny implementation of CRC-32.
 *
 * Written by Solar Designer <solar at openwall.com> in 1998, revised in
 * 2005 for use in John the Ripper, and placed in the public domain.
 * There's absolutely no warranty.
 */

#include <stdio.h>

#include "crc32.h"

#define POLY 0xEDB88320
#define ALL1 0xFFFFFFFF

/* static crc32_t *crc32_table = NULL; */
static crc32_t crc32_table[0x100];
static char crc32_table_init = 0;

void crc32_init(crc32_t *value)
{
    unsigned int index, bit;
    crc32_t entry;

    *value = ALL1;

    if (crc32_table_init) return;
    crc32_table_init = 1;
    /* crc32_table = malloc(sizeof(*crc32_table) * 0x100); */

    for (index = 0; index < 0x100; index++) {
        entry = index;

        for (bit = 0; bit < 8; bit++)
        if (entry & 1) {
            entry >>= 1;
            entry ^= POLY;
        } else
            entry >>= 1;

        crc32_table[index] = entry;
    }
}


void crc32_update(crc32_t *value, const void *data, const unsigned int size)
{
    unsigned char *ptr;
    unsigned int count;
    crc32_t result;

    result = *value;
    ptr = (unsigned char *) data;
    count = size;

    if (count)
    do {
        result = (result >> 8) ^ crc32_table[(result ^ *ptr++) & 0xFF];
    } while (--count);

    *value = result;
}


void crc32_final(crc32_t *value, unsigned char *out)
{
    *value = ~(*value);
    out[3] = *value;
    out[2] = *value >> 8;
    out[1] = *value >> 16;
    out[0] = *value >> 24;
}


void crc32_final2(crc32_t *value, unsigned char *out)
{
    *value = ~(*value);
    out[0] = *value;
    out[1] = *value >> 8;
    out[2] = *value >> 16;
    out[3] = *value >> 24;
}
