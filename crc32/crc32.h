/*
 * This is a tiny implementation of CRC-32.
 *
 * Written by Solar Designer <solar at openwall.com> in 1998, revised in
 * 2005 for use in John the Ripper, and placed in the public domain.
 * There's absolutely no warranty.
 */

#ifndef _JOHN_CRC32_H
#define _JOHN_CRC32_H

typedef unsigned int crc32_t;

extern void crc32_init(crc32_t *value);
#define crc32_start crc32_init

extern void crc32_update(crc32_t *value, const void *data, const unsigned int size);
#define crc32_process crc32_update

extern void crc32_final(crc32_t *value, unsigned char *out);
#define crc32_finish

extern void crc32_final2(crc32_t *value, unsigned char *out);
#define crc32_finish2

#endif
