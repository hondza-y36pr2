/******************************************************************************
 * setrlimit.c: execute program in with restricted resources
 * Written by hondza. Public domain.
 *****************************************************************************/

#define _BSD_SOURCE

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include <getopt.h>

#include <sys/time.h>
#include <sys/resource.h>


#define SET_SOFT 1
#define SET_HARD 2

static int both = 0;
static int relaxed = 0;


void help()
{
    fputs("Usage: setrlimit [options] -- /path/to/executable [arguments]\n", stderr);
    fputs("\n(lower case sets soft limit, UPPER case hard limit)\n\n", stderr);
    fputs("Options:\n", stderr);
    fputs("-n <no>, -N <no>\tNOFILE\t\tmax file descriptors+1\n", stderr);
    fputs("-p <no>, -P <no>\tNPROC\t\tmax number of processes\n", stderr);
    fputs("-u <no>, -U <no>\tNPROC\t\tmax number of processes\n", stderr);
    fputs("-c <no>, -C <no>\tCORE\t\tmax core file size (bytes)\n", stderr);
    fputs("-f <no>, -F <no>\tFSIZE\t\tmax size of files written\n", stderr);
    fputs("-i <no>, -I <no>\tSIGPENDING\tmax pending signals\n", stderr);
    fputs("-s <no>, -S <no>\tSTACK\t\tmax stack size (bytes)\n", stderr);
    fputs("-t <no>, -T <no>\tCPU\t\tmax cpu usage (seconds)\n", stderr);
    fputs("-v <no>, -V <no>\tAS\t\tmax address space (bytes)\n\n", stderr);
    fputs("-h\tthis help\n", stderr);
    fputs("-r\trelaxed ({get,set}rlimit() mail fail)\n", stderr);
    fputs("-b\tset both soft and HARD limit\n", stderr);
    fputs("-x\tclose all file descriptors before execv()\n\n", stderr);
} /* help() */


void do_set(const int resource, const char *arg, int opt)
{
    struct rlimit lim;
    int err;
    unsigned long l;

    opt = both ? (SET_SOFT | SET_HARD) : (opt);

    /* If I'm setting both, I don't need to know current ones */
    if(!both)
    {
        err = getrlimit(resource, &lim);
        if(-1 == err)
        {
            if(!relaxed)
            {
                perror("getrlimit failed");
                exit(1);
            }
            else
            {
                /* FIXME */
                lim.rlim_cur = lim.rlim_max = RLIM_INFINITY;
            }
        }
    }

    l = strtoul(arg, NULL, 10);

    if(opt & SET_SOFT) lim.rlim_cur = l;
    if(opt & SET_HARD) lim.rlim_max = l;

    err = setrlimit(resource, &lim);
    if(-1 == err && !relaxed)
    {
        perror("setrlimit failed");
        exit(1);
    }
} /* do_set() */



int main(int argc, char ** argv)
{
    int c, i, doclose=0;

    if(argc < 2)
    {
        help();
        exit(1);
    }

    while( (c = getopt(argc, argv, "n:N:p:P:c:C:f:F:i:I:s:S:t:T:u:U:v:V:rbhx")) != -1 )
    {
        switch(c)
        {
            case 'b': 
                both = 1;
                break;

            case 'r': 
                relaxed = 1;
                break;

            case 'x':
                doclose = 1;
                break;

            case 'h':
                help();
                exit(0);
                break;

            case 'n': 
                do_set(RLIMIT_NOFILE, optarg, SET_SOFT);
                break;
            case 'N': 
                do_set(RLIMIT_NOFILE, optarg, SET_HARD);
                break;

            case 'p': case 'u':
                do_set(RLIMIT_NPROC, optarg, SET_SOFT);
                break;
            case 'P': case 'U':
                do_set(RLIMIT_NPROC, optarg, SET_HARD);
                break;

            case 'c': 
                do_set(RLIMIT_CORE, optarg, SET_SOFT);
                break;
            case 'C': 
                do_set(RLIMIT_CORE, optarg, SET_HARD);
                break;

            case 'f': 
                do_set(RLIMIT_FSIZE, optarg, SET_SOFT);
                break;
            case 'F': 
                do_set(RLIMIT_FSIZE, optarg, SET_HARD);
                break;

            case 'i': 
                do_set(RLIMIT_SIGPENDING, optarg, SET_SOFT);
                break;
            case 'I': 
                do_set(RLIMIT_SIGPENDING, optarg, SET_HARD);
                break;

            case 's': 
                do_set(RLIMIT_STACK, optarg, SET_SOFT);
                break;
            case 'S': 
                do_set(RLIMIT_STACK, optarg, SET_HARD);
                break;

            case 't': 
                do_set(RLIMIT_CPU, optarg, SET_SOFT);
                break;
            case 'T': 
                do_set(RLIMIT_CPU, optarg, SET_HARD);
                break;

            case 'v': 
                do_set(RLIMIT_AS, optarg, SET_SOFT);
                break;
            case 'V': 
                do_set(RLIMIT_AS, optarg, SET_HARD);
                break;


            default: help(); exit(1);
        } /* switch argument */
    } /* while getopt() */

    if(doclose)
        for ((i = getdtablesize()); i >= 0 ; i--) close(i);

    if(optind < argc)
    {
        c = execv(argv[optind], argv+optind);
        if(!doclose && -1 == c)
        {
            perror("execv() failed");
        }
        exit(1);
    }
    else if(!doclose)
    {
        fputs("FATAL: nothing to exec!\n", stderr);
        exit(1);
    }

    return 0;
} /* main() */

