/******************************************************************************
 * nodevrandom.c
 *****************************************************************************/

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <stdarg.h>
#include <dlfcn.h>

#include <string.h>

#include "nodevrandom.h"

static real_open_t real_open;
static real_open_t real_open64;
static real_fopen_t real_fopen;
static real_fopen_t real_fopen64;

static int nodevrandom_init_done=0;


static void lib_init(void)
{
    if(nodevrandom_init_done) return;
    nodevrandom_init_done=1;

    *(void **) (&real_open) = dlsym(RTLD_NEXT, "open");
    if(!real_open)
    {   
        fprintf(stderr, "(NODEVRANDOM) Cannot load symbol 'open' %s\n", dlerror());
        _exit(1);
    }
    *(void **) (&real_open64) = dlsym(RTLD_NEXT, "open64");
    if(!real_open64)
    {   
        fprintf(stderr, "(NODEVRANDOM) Cannot load symbol 'open64' %s\n", dlerror());
        _exit(1);
    }
    *(void **) (&real_fopen) = dlsym(RTLD_NEXT, "fopen");
    if(!real_fopen)
    {   
        fprintf(stderr, "(NODEVRANDOM) Cannot load symbol 'fopen' %s\n", dlerror());
        _exit(1);
    }
    *(void **) (&real_fopen64) = dlsym(RTLD_NEXT, "fopen64");
    if(!real_fopen64)
    {   
        fprintf(stderr, "(NODEVRANDOM) Cannot load symbol 'fopen64' %s\n", dlerror());
        _exit(1);
    }
} /* lib_init() */




int open(const char *pathname, int flags, ...)
{
    va_list list;
    mode_t mode;
    
    if(!nodevrandom_init_done) lib_init();

    if(0 == strcmp(pathname, "/dev/random"))
    {
        va_start(list, flags);
        mode = va_arg(list, mode_t);
        va_end(list);
        return real_open("/dev/urandom", flags, mode);
    }
    else
        return real_open(pathname, flags);
}


int open64(const char *pathname, int flags, ...)
{
    va_list list;
    mode_t mode;
    
    if(!nodevrandom_init_done) lib_init();

    if(0 == strcmp(pathname, "/dev/random"))
    {
        va_start(list, flags);
        mode = va_arg(list, mode_t);
        va_end(list);
        return real_open("/dev/urandom", flags, mode);
    }
    else
        return real_open(pathname, flags);
}



FILE *fopen(const char *path, const char *mode)
{
    if(!nodevrandom_init_done) lib_init();

    if(0 == strcmp(path, "/dev/random"))
        return real_fopen("/dev/urandom", mode);
    else 
        return real_fopen(path, mode);
}




FILE *fopen64(const char *path, const char *mode)
{
    if(!nodevrandom_init_done) lib_init();

    if(0 == strcmp(path, "/dev/random"))
        return real_fopen("/dev/urandom", mode);
    else 
        return real_fopen(path, mode);
}

