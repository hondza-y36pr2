/******************************************************************************
 * nodevrandom.h
 *****************************************************************************/

#ifndef _NODEVRANDOM_H_
#define _NODEVRANDOM_H_

typedef int (*real_open_t) (const char *, int, ...);

typedef FILE *(*real_fopen_t) (const char *, const char *);

static void lib_init(void) __attribute__((constructor));

#endif /* _NODEVRANDOM_H_ */


