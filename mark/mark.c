/******************************************************************************
 * mark.c
 *****************************************************************************/

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <dlfcn.h>

#include "mark.h"


#define SO_MARK 36


real_socket_t real_socket;
int default_mark = 1;


void lib_init(void)
{
    char *temp;

    *(void **) (&real_socket) = dlsym(RTLD_NEXT, "socket");
    if(!real_socket)
    {   
        fprintf(stderr, "Cannot load symbol 'socket' %s\n", dlerror());
        _exit(1);
    }

    temp = getenv("MARK");
    if(temp) default_mark = (int) strtoul(temp, NULL, 10);
}


int socket(int domain, int type, int protocol)
{
    int err2, mark = default_mark;
    int err = real_socket(domain, type, protocol);

    if(domain != PF_INET || err == -1) return err;

    err2 = setsockopt(err, SOL_SOCKET, SO_MARK, (void *) &mark, sizeof (int));
    if(err2 == -1) { perror("setsockopt(... SO_MARK ...)"); exit(1); }

    return err;
}


