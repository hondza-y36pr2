/******************************************************************************
 * newns.c
 * Roughly the same as: http://glandium.org/blog/?p=217
 *****************************************************************************/

#include <sched.h>
#include <syscall.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <sys/syscall.h>
#include <sys/wait.h>

#include <getopt.h>


void help()
{
    fputs("newns: launch a process in a new namespace\n\n" \
    "Usage: newns [-0npuUi] [command]\n\n" \
    "Options:\n" \
    "\t -0\t\treset previous (including default CLONE_NEWNS)\n" \
    "\t -n\t\t[CLONE_NEWNS]   new mount namespace; the default\n" \
    "\t -p\t\t[CLONE_NEWPID]  new PID namespace\n" \
    "\t -U\t\t[CLONE_NEWUTS]  new UTS namespace\n" \
    "\t -u\t\t[CLONE_NEWUSER] new USER namespace\n" \
    "\t -i\t\t[CLONE_NEWIPC]  new IPC namespace\n\n" \
    "Default namespace is mount namespace (CLONE_NEWNS); default command is a new shell (/bin/sh)\n\n" \
    , stderr);
}


int main(int argc, char *argv[]) 
{
    int err, c, flag=CLONE_NEWNS;

    while( (c = getopt(argc, argv, "0unUiph")) != -1 )
    {
        switch(c)
        {
            case '0': flag = 0; break;
            case 'n': flag |= CLONE_NEWNS; break;
            case 'u': flag |= CLONE_NEWUSER; break;
            case 'U': flag |= CLONE_NEWUTS; break;
            case 'p': flag |= CLONE_NEWPID; break;
            case 'i': flag |= CLONE_NEWIPC; break;
            case 'h': help(); exit(0); break;
            case '?': help(); exit(1); break;
        } /* switch argument */
    } /* while getopt() */

#if 0
    err = syscall(SYS_unshare, flag);
    /* err = unshare(flag); */
    if(-1 == err)
    {
        perror("unshare() failed");
        exit(1);
    }
#else
    err = syscall(SYS_clone, SIGCHLD | flag, NULL); 
    if(-1 == err)
    {
        perror("clone() failed");
        exit(1);
    }
    else if(0 != err)
    {
       wait(NULL);
       return 0;
    }
#endif

    if(optind < argc)
        return execvp(argv[optind], &argv[optind]);

    return execv("/bin/sh", NULL);
}
