/* getdelim() and getline() from dietlibc. GPLv2 */

#ifdef NEED_GETLINE

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <sys/types.h>

#include "getline.h"

ssize_t getdelim(char **lineptr, size_t *n, int delim, FILE *stream) {
  size_t i;
  int x, tmp;
  char *new;

  if (!lineptr || !n) {
    errno=EINVAL;
    return -1;
  }
  if (!*lineptr) *n=0;
  for (i=0; ; ) {
    x=fgetc(stream);
    if (i>=*n) {
      tmp=*n+100;
      new=realloc(*lineptr,tmp);
      if (!new) return -1;
      *lineptr=new; *n=tmp;
    }
    if (x==EOF) { if (!i) return -1; (*lineptr)[i]=0; return i; }
    (*lineptr)[i]=x;
    ++i;
    if (x==delim) break;
  }
  (*lineptr)[i]=0;
  return i;
}

ssize_t getline(char **lineptr, size_t *n, FILE *stream) {
  return getdelim(lineptr,n,'\n',stream);
}

#endif /* NEED_GETLINE */


