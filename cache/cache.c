/******************************************************************************
 * cache.c
 *****************************************************************************/

#define _FILE_OFFSET_BITS 64
#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <fcntl.h>

#include <getopt.h> /* solaris wants it */

#include <errno.h>

#define STRINGIFY(x) #x


static unsigned long long get_special_number(char *in)
{
    unsigned long long ret;
    char *unit;

    ret = strtoull(in, &unit, 10);
    if(*unit == 'k') { ret *= 1024; }
    else if(*unit == 'K') { ret *= 1000; }
    else if(*unit == 'm') { ret *= 1024*1024; }
    else if(*unit == 'M') { ret *= 1000*1000; }
    else if(*unit == 'g') { ret *= 1024*1024*1024; }
    else if(*unit == 'G') { ret *= 1000*1000*1000; }

    return ret;
} /* get_special_number() */


int main(int argc, char ** argv)
{
    int err, i, ret=0, fd, c, advice=POSIX_FADV_DONTNEED, verbose=0;
    char *adv_string = STRINGIFY(POSIX_FADV_DONTNEED);
    off_t len=0, offset=0;

    while( (c = getopt(argc, argv, "a:nsrNwdo:l:v")) != -1 )
    {
        switch(c)
        {
            case 'v': verbose = 1; break;
            
            case 'o': offset = get_special_number(optarg); break;
            case 'l': len = get_special_number(optarg); break;

            case 'n': advice = POSIX_FADV_NORMAL; adv_string = STRINGIFY(POSIX_FADV_NORMAL); break;
            case 's': advice = POSIX_FADV_SEQUENTIAL; adv_string = STRINGIFY(POSIX_FADV_SEQUENTIAL); break;
            case 'r': advice = POSIX_FADV_RANDOM; adv_string = STRINGIFY(POSIX_FADV_RANDOM); break;
            case 'N': advice = POSIX_FADV_NOREUSE; adv_string = STRINGIFY(POSIX_FADV_NOREUSE); break;
            case 'w': advice = POSIX_FADV_WILLNEED; adv_string = STRINGIFY(POSIX_FADV_WILLNEED); break;
            case 'd': advice = POSIX_FADV_DONTNEED; adv_string = STRINGIFY(POSIX_FADV_DONTNEED); break;

            case 'a': 
                if(strcasestr(optarg, "NORM")) { advice = POSIX_FADV_NORMAL; adv_string = STRINGIFY(POSIX_FADV_NORMAL); }
                else if(strcasestr(optarg, "SEQ")) { advice = POSIX_FADV_SEQUENTIAL; adv_string = STRINGIFY(POSIX_FADV_SEQUENTIAL); }
                else if(strcasestr(optarg, "RAND")) { advice = POSIX_FADV_RANDOM; adv_string = STRINGIFY(POSIX_FADV_RANDOM); }
                else if(strcasestr(optarg, "NORE")) { advice = POSIX_FADV_NOREUSE; adv_string = STRINGIFY(POSIX_FADV_NOREUSE); }
                else if(strcasestr(optarg, "WILL")) { advice = POSIX_FADV_WILLNEED; adv_string = STRINGIFY(POSIX_FADV_WILLNEED); }
                else if(strcasestr(optarg, "DONT")) { advice = POSIX_FADV_DONTNEED; adv_string = STRINGIFY(POSIX_FADV_DONTNEED); }
                else
                {
                    fprintf(stderr, "Not a valid advice ('%s'), defaulting to POSIX_FADV_DONTNEED\n", optarg);
                    advice = POSIX_FADV_DONTNEED;
                    adv_string = STRINGIFY(POSIX_FADV_DONTNEED);
                }
                break;

        } /* switch argument */
    } /* while getopt() */


    if(optind >= argc)
    {
        fprintf(stderr, "Usage: cache [-o offset[kKmMgG]] [-l length[kKmMgG]] [-a NORM|SEQ|RAND|NORE|WILL|DONT] [-nsrNwd] <filename> [filenames ...]\n\n");
        exit(1);
    }

    if(verbose)
        fprintf(stderr, "Will use %s (offset=%llu, length=%llu)\n", adv_string, offset, len);
        
    for(i=optind; i < argc; i++)
    {
        fd = open(argv[i], O_RDONLY);
        if(-1 == fd)
        {
            ret = 1;
            fprintf(stderr, "%s: open() failed: %s\n", argv[i], strerror(errno));
            continue;            
        }
        err = posix_fadvise(fd, offset, len, advice);
        if(-1 == err)
        {
            ret = 1;
            fprintf(stderr, "%s: posix_fadvise() failed: %s\n", argv[i], strerror(errno));
        }
        close(fd);
    } /* for i */

    return ret;
} /* main() */

