/******************************************************************************
 * {sha256,sha1,md5,crc32}sum, depends on argv[0] (default is sha256sum).
 * Uses libtomcrypt (public domain) and dietlibc's getline() ... GLPv2.
 * Due to GPLv2 virus it is GPLv2.
 *****************************************************************************/

#define _FILE_OFFSET_BITS 64
#define _GNU_SOURCE

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#include <getopt.h> /* solaris wants it */

#include <tomcrypt.h>

#include "crc32.h"


#ifdef WIN32
#define FOPEN_MODE "rb"
#define BUFSIZE_CONST
#else
#define FOPEN_MODE "r"
#include <sys/stat.h>
#endif /* WIN32 */

#ifdef NEED_GETLINE
#include "getline.h"
#endif /* NEED_GETLINE */


#define DEFAULT_BL 4096
#define DEFAULT_LINE_LEN 4096

#define MAKE_CHECKSUM_ERR_OK   0
#define MAKE_CHECKSUM_ERR_OPEN 1
#define MAKE_CHECKSUM_ERR_READ 3
#define MAKE_CHECKSUM_ERR_MEM  4

#define MD5 1
#define SHA1 2
#define SHA256 4
#define CRC32 8
#define HASH_ALL (MD5|SHA1|SHA256|CRC32)


#define RELEASE "r2"


static const unsigned char hexchar[] = "0123456789abcdef";


/* long time ago there was a number of options :) */
struct t_options {
    int hashes;
    int check_selected;
} opt; 

static int verbose=0;
static int comments=1;

struct hash_out {
    char *filename;
    int hashes;
    crc32_t crc32;
    hash_state md5;
    hash_state sha1;
    hash_state sha256;
    off_t written;
    unsigned char md5_o[16];
    unsigned char sha1_o[20];
    unsigned char sha256_o[32];
    unsigned char crc32_o[4];
};




static void bin2hex(unsigned char *out, unsigned char *in, size_t size)
{
    for(; size; size--, in++)
    {
        *out = hexchar[((*in)>>4)]; out++;
        *out = hexchar[((*in)&15)]; out++;
    }
    *out = '\0';
} /* bin2hex() */



static void hex2bin(unsigned char *out, unsigned char *in, size_t size)
{   
    for(; size; size-=2, in+=2, out++)
    {   
        *out = ((((*in) >= 'a' && (*in) <= 'f') ?
            ( (*in) - 'a' + 10 )
            : ( ((*in) >= 'A' && (*in) <= 'F')
                ? ((*in) - 'A' + 10)
                : ((*in) - '0'))) << 4)
            +
           ((((*(in+1)) >= 'a' && (*(in+1)) <= 'f') ?
                    ( (*(in+1)) - 'a' + 10 )
                    : ( ((*(in+1)) >= 'A' && (*(in+1)) <= 'F')
                        ? ((*(in+1)) - 'A' + 10)
                        : ((*(in+1)) - '0'))) & 15);
    }
} /* hex2bin() */



static int is_hex_string(char *in, size_t len)
{
    for(; len; len--, in++)
        if(!isxdigit(*in)) return 0;
    return 1;
} /* is_hex_string() */




static size_t getbufsize(FILE *f)
{
#ifdef BUFSIZE_CONST
    return DEFAULT_BL;
#else
    struct stat st;
    int err;
    err = fstat(fileno(f), &st);
    if(err == -1 || !st.st_blksize) return DEFAULT_BL;
    return st.st_blksize;
#endif
} /* getbufsize() */




static int mark_filename_end(char *in)
{
    int i;
    for(i=0; *in != '\0' && *in != '\n' && *in != '\r'; in++, i++)
        ;
    *in = '\0';
    return i;
} /* mark_filename_end() */




static int mark_hash_end(char *in, char *c)
{
    int i;
    for(i=0; isxdigit(*in); in++, i++)
        ;
    *c = *in;
    *in = '\0';
    return i;
} /* mark_hash_end() */




static int make_checksum(struct hash_out *out)
{
    size_t err, blks;
    unsigned char *data;
    FILE *f;

    if(!(out->filename)) f = stdin;
    else
    {
        f = fopen(out->filename, FOPEN_MODE);
        if(!f) return MAKE_CHECKSUM_ERR_OPEN;
    }

    blks = getbufsize(f);
    data = malloc(blks);
    if(!data) 
    {
        fclose(f);
        return MAKE_CHECKSUM_ERR_MEM;
    }

    /* init hashes */
    if(out->hashes & CRC32) crc32_init(&(out->crc32));
    if(out->hashes & MD5) md5_init(&(out->md5));
    if(out->hashes & SHA1) sha1_init(&(out->sha1));
    if(out->hashes & SHA256) sha256_init(&(out->sha256));
    
    while(1)
    {
        err = fread(data, 1, blks, f);
        /* process */
        if(err > 0)
        {
            out->written += err;
            if(out->hashes & CRC32) crc32_update(&(out->crc32), data, err);
            if(out->hashes & MD5) md5_process(&(out->md5), data, err);
            if(out->hashes & SHA1) sha1_process(&(out->sha1), data, err);
            if(out->hashes & SHA256) sha256_process(&(out->sha256), data, err);
        }
        else if(feof(f)) break;
        else 
        { 
            if(out->hashes & CRC32) crc32_final(&(out->crc32), out->crc32_o);
            if(out->hashes & MD5) md5_done(&(out->md5), out->md5_o);
            if(out->hashes & SHA1) sha1_done(&(out->sha1), out->sha1_o);
            if(out->hashes & SHA256) sha256_done(&(out->sha256), out->sha256_o);
            fclose(f);
            memset(data, 0, blks); free(data);
            return MAKE_CHECKSUM_ERR_READ;
        }
    } /* while 1 */

    fclose(f);
    memset(data, 0, blks); free(data);
    if(out->hashes & CRC32) crc32_final(&(out->crc32), out->crc32_o);
    if(out->hashes & MD5) md5_done(&(out->md5), out->md5_o);
    if(out->hashes & SHA1) sha1_done(&(out->sha1), out->sha1_o);
    if(out->hashes & SHA256) sha256_done(&(out->sha256), out->sha256_o);

    return MAKE_CHECKSUM_ERR_OK;
} /* make_checksum() */




void print_hashes(struct hash_out *in)
{
    unsigned char hex_form[65];
    unsigned char *bin_form;
    
    hex_form[64] = '0';

    #define print_hash(a,b) do { \
        bin2hex(hex_form, bin_form, (a)); \
        if(comments) printf("# R %s %llu %*s  %s\n", (b), in->written, 2*(a), hex_form, in->filename ? in->filename : "-"); \
        printf("%*s  %s\n", 2*(a), hex_form, in->filename ? in->filename : "-"); \
        } while(0)

    if(opt.hashes & CRC32) 
    {
        bin_form = in->crc32_o;
        print_hash(sizeof(in->crc32_o), "CRC32");
    }
    if(opt.hashes & MD5) 
    {
        bin_form = in->md5_o;
        print_hash(sizeof(in->md5_o), "MD5");
    }
    if(opt.hashes & SHA1)
    {
        bin_form = in->sha1_o;
        print_hash(sizeof(in->sha1_o), "SHA1");
    }
    if(opt.hashes & SHA256)
    {
        bin_form = in->sha256_o;
        print_hash(sizeof(in->sha256_o), "SHA256");
    }
 } /* print_hash() */




/* depends on "char *filename" */
#define handle_make_checksum_err(x) do { \
    if((x) != MAKE_CHECKSUM_ERR_OK)  \
    { \
        switch((x)) \
        { \
            case MAKE_CHECKSUM_ERR_OPEN: \
                fprintf(stderr, "fopen(\"%s\") failed: %s\n", filename, \
                        strerror(errno)); \
                return 2; \
            case MAKE_CHECKSUM_ERR_MEM: \
                fprintf(stderr, "malloc() failed, file: '%s'\n",  \
                        filename); \
                return 2; \
            case MAKE_CHECKSUM_ERR_READ: \
                fprintf(stderr, "fread(\"%s\") failed: %s\n", filename,  \
                        strerror(errno)); \
                return 2; \
        } /* switch err */ \
    } \
 } while(0)



int do_hash(char *filename)
{
    int err;
    struct hash_out out;
    unsigned char hex_form[64];

    hex_form[64] = '\0';

    memset(&out, 0, sizeof(out));
    out.hashes = opt.hashes;
    out.filename = filename;
    
    err = make_checksum(&out);
    handle_make_checksum_err(err);

    print_hashes(&out);
   
    return 0;
} /* do_hash() */


#define check_failed(x, y) do { printf("%s: FAILED (%s)\n", ((x) ? (x) : "(stdin)"), (y)); } while(0)
#define check_ok(x, y) do { if(verbose) printf("%s: OK (%s)\n", ((x) ? (x) : "(stdin)"), (y)); } while(0)


int check_file(struct hash_out *what, struct hash_out *against)
{
    char *filename = what->filename;
    int err, retval=0;

    err = make_checksum(what);
    handle_make_checksum_err(err);
    
    if(what->hashes & CRC32)
    {
        if((memcmp(what->crc32_o, against->crc32_o, sizeof(what->crc32_o))) != 0) { check_failed(what->filename, "CRC32"); retval = 1; }
        else check_ok(what->filename, "CRC32");
    }
    if(what->hashes & MD5)
    {
        if((memcmp(what->md5_o, against->md5_o, sizeof(what->md5_o))) != 0) { check_failed(what->filename, "MD5"); retval = 1; }
        else check_ok(what->filename, "MD5");
    }
    if(what->hashes & SHA1)
    {
        if((memcmp(what->sha1_o, against->sha1_o, sizeof(what->sha1_o))) != 0) { check_failed(what->filename, "SHA1"); retval = 1; }
        else check_ok(what->filename, "SHA1");
    }
    if(what->hashes & SHA256)
    {
        if((memcmp(what->sha256_o, against->sha256_o, sizeof(what->sha256_o))) != 0) { check_failed(what->filename, "SHA256"); retval = 1; }
        else check_ok(what->filename, "SHA256");
    }

    return retval;
} /* check_file() */




int do_hash_check(char *in_filename)
{
    FILE *f;
    char *temp_line, *filename;
    char sep;
    int retval=0, current_hash=0;
    ssize_t err2;
    size_t line_len = DEFAULT_LINE_LEN, lineno, hex_len, filename_len;
    struct hash_out temp_out, to_compare;

    memset(&temp_out, 0, sizeof(temp_out));
    memset(&to_compare, 0, sizeof(to_compare));

    if(!in_filename) f = stdin;
    else
    {
        f = fopen(in_filename, FOPEN_MODE);
        if(!f) 
        { 
            fprintf(stderr, "fopen(\"%s\") failed: %s\n", in_filename,
                    strerror(errno));
            return 2;
        }
    }
    
    temp_line = malloc(DEFAULT_LINE_LEN);
    if(!temp_line)
    {
        fprintf(stderr, "malloc() failed, file: '%s'\n", in_filename ? in_filename : "(stdin)");
        if(f != stdin) fclose(f);
        return 2;
    }


    for(lineno=1; ;lineno++)
    {
        hex_len=0;
        filename=NULL;

        err2 = getline(&temp_line, &line_len, f);
        if(err2 < 1)
        {
            /* we may have a pending check to do */
            if(temp_out.hashes)
            {
                /* make checksum */
                retval |= check_file(&temp_out, &to_compare);
                /* clean */
                if(temp_out.filename) free(temp_out.filename);
                memset(&temp_out, 0, sizeof(temp_out));
                memset(&to_compare, 0, sizeof(to_compare));
            }

            if(!feof(f)) retval |= 2;
            if(temp_line) { memset(temp_line, 0, line_len); free(temp_line); }
            fclose(f);
            return retval;
        }

        /* skip comment */
        if(*temp_line == '#' || *temp_line == ';') continue;

        if(!(filename = strstr(temp_line, "  "))) filename = strstr(temp_line, " *");

        if(!filename)
        {
            hex_len = mark_hash_end(temp_line, &sep);
            /* skip malformed line */
            if(!(sep == '\n' || sep == '\r' || sep == '\0'))
            {
                if(verbose) fprintf(stderr, "malformed (sep) line %d in file: '%s'\n", lineno, in_filename ? in_filename : "(stdin)");
                continue;
            }
        }
        else
        {
            *filename='\0';
            hex_len = filename - temp_line;
            if(!is_hex_string(temp_line, hex_len))
            {
                if(verbose) fprintf(stderr, "malformed (!is_hex_string) line %d in file: '%s'\n", lineno, in_filename ? in_filename : "(stdin)");
                continue;
            }
        }

        switch(hex_len)
        {
            case 8: current_hash = CRC32; break;
            case 32: current_hash = MD5; break;
            case 40: current_hash = SHA1; break;
            case 64: current_hash = SHA256; break;
            default:
            {
                if(verbose) fprintf(stderr, "malformed (hex_len) line %d in file: '%s'\n", lineno, in_filename ? in_filename : "(stdin)");
                continue;
            }
        }
        
        /* skip on '-C' and non-selected hash */
        if(opt.check_selected && !(current_hash & opt.hashes)) continue;

        if(filename)
        {
            filename += 2;
            filename_len = mark_filename_end(filename);
            /* XXX: perhaps allow it as stdin? */
            if(!filename_len)
            {
                if(verbose) fprintf(stderr, "malformed (nothing_after_sep) line %d in file: '%s'\n", lineno, in_filename ? in_filename : "(stdin)");
            }
            else if(filename_len == 1 && *filename == '-') filename = NULL;
        }


#define modify_to_compare() do { \
    switch(current_hash) \
    { \
        case CRC32: hex2bin(to_compare.crc32_o, (unsigned char *)temp_line, hex_len); break; \
        case MD5: hex2bin(to_compare.md5_o, (unsigned char *)temp_line, hex_len); break; \
        case SHA1: hex2bin(to_compare.sha1_o, (unsigned char *)temp_line, hex_len); break; \
        case SHA256: hex2bin(to_compare.sha256_o, (unsigned char *)temp_line, hex_len); break; \
    } \
  } while(0)



#define allocate_filename() do { \
    if(filename)  \
    { \
        temp_out.filename = to_compare.filename = strdup(filename); \
        if(!temp_out.filename) \
        { \
            perror("strdup() failed"); \
            if(f != stdin) fclose(f); \
            return 2; \
        } \
    } \
 } while(0)


        /* first */
        if(!temp_out.hashes)
        {
            allocate_filename();
            temp_out.hashes = to_compare.hashes = current_hash;
            modify_to_compare();
        } /* !temp_out.hashes */
        else /* not first */
        {
            /* now I need to compare filenames */
            if( (!filename && !temp_out.filename) || (filename && temp_out.filename && (strcmp(filename, temp_out.filename) == 0)) )
            {
                /* same files, just bitwise OR the hash */
                temp_out.hashes |= current_hash;
                to_compare.hashes = temp_out.hashes;
                modify_to_compare();
            }
            else /* different files => make checksums & re-init temp_out */
            {
                /* make checksum */
                retval |= check_file(&temp_out, &to_compare);

                /* clean */
                if(temp_out.filename) free(temp_out.filename);
                memset(&temp_out, 0, sizeof(temp_out));
                memset(&to_compare, 0, sizeof(to_compare));

                /* re-init */
                allocate_filename();
                temp_out.hashes = to_compare.hashes = current_hash;
                modify_to_compare();
            }
        } /* not first */

    } /* for lineno */

    return retval;
} /* do_hash_check() */





static void help()
{
    fputs("{sha256,sha1,md5,crc32}sum: generates or verifies checksums (message digests)\n", stderr);
    fputs("Usage: {sha256,sha1,md5,crc32}sum [-01235CachqvV] [file...] \n\n", stderr);
    fputs("Options:\n"
            "\t-c\t\tcheck mode\n"
            "\t-C\t\tcheck mode (check against selected hashes)\n"
            "\t-0\t\tZero previous hashes\n"
            "\t-1\t\tSHA1\n"
            "\t-2\t\tSHA256\n"
            "\t-3\t\tCRC32\n"
            "\t-5\t\tMD5\n"
            "\t-a\t\tall hashes\n"
            "\t-m <number>\t\tselected hashes\n"
            "\t-h\t\thelp\n"
            "\t-v\t\tverbose\n"
            "\t-q\t\tcreate comments\n"
            "\t-V\t\tversion\n"
            "\n", stderr);
} /* help() */




static void version()
{
    fprintf(stderr, "s2-%s\n\n", RELEASE);
} /* version() */




/* samotny programek */
int main(int argc, char ** argv)
{
    int check = 0, retval = 0, c, temp;
    
    opt.hashes = SHA256;
    opt.check_selected = 0;

    if( (strstr(argv[0], "s1")) || (strstr(argv[0], "sha1sum")) ) opt.hashes = SHA1;
    else if( (strstr(argv[0], "s2")) || (strstr(argv[0], "sha256sum")) ) opt.hashes = SHA256;
    else if( (strstr(argv[0], "md5")) ) opt.hashes = MD5;
    else if( (strstr(argv[0], "crc32")) ) opt.hashes = CRC32;
    else if( (strstr(argv[0], "md")) ) opt.hashes = HASH_ALL;
    else opt.hashes = 0;

    while( (c = getopt(argc, argv, "ahvVcCq01235m:")) != -1 )
    {
        switch(c)
        {
            case 'c': check = 1; break;
            case 'q': comments = 0; break;
            case 'C': check = opt.check_selected = 1; break;
            case 'v': verbose=1; break;
            case 'V': version(); exit(0);
            case 'h': help(); exit(0);
            case 'a': opt.hashes = HASH_ALL; break;
            case '0': opt.hashes = 0; break;
            case '2': opt.hashes |= SHA256; break;
            case '1': opt.hashes |= SHA1; break;
            case '3': opt.hashes |= CRC32; break;
            case '5': opt.hashes |= MD5; break;
            case 'm': temp = strtoul(optarg, NULL, 10); if((temp & HASH_ALL)) opt.hashes = temp; break;
            default: help(opt.hashes); exit(1);
        } /* switch argument */
    } /* while getopt() */

    if(!(opt.hashes & HASH_ALL)) opt.hashes = SHA256;

    if(optind < argc)
    {
        if(check)
        {
            while(optind < argc)
                retval |= (do_hash_check(argv[optind++])); 
        } /* if check */
        else
        {
            while(optind < argc)
                retval |= (do_hash(argv[optind++]));
        } /* not check */

        return retval;
    }
    else
    {
        if(check) { retval = do_hash_check(NULL); }
        else 
        { retval = (do_hash(NULL)); }

        return retval;
    } /* no file => stdin */


    /* 
     * 0 .. OK, 1 .. bad checksum, 2 .. other error (fopen(), ...) 
     * bitwise ORed
     */
    return retval;
} /* main() */

