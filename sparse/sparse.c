/******************************************************************************
 * sparse.c
 *****************************************************************************/

#define _FILE_OFFSET_BITS 64
#define _GNU_SOURCE

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <getopt.h>

#define THRESHOLD 1023

void help()
{
    fputs("sparse: create a sparse file from stdin\n\n", stderr);
    fputs("Usage: sparse [-v] [-c threshold] <output_file>\n\n", stderr);
} /* help() */


int main(int argc, char ** argv)
{
    int c, i, verbose=0;
    long offset=0;
    unsigned long threshold = THRESHOLD, seeks=0;
    FILE *fo;

    while( (c = getopt(argc, argv, "vhc:")) != -1 )
    {
        switch(c)
        {
            case 'c': 
                /* 0 is acceptable */
                threshold = strtoul(optarg, NULL, 10);
                break;

            case 'v':
                verbose = 1;
                break;

            case 'h':
                help();
                return 0;
            
            default:
                help();
                return 1;
        }
    }


    if(optind >= argc)
    {
        help();
        return 1;
    }

    fo = fopen(argv[optind], "w");
    if(!fo)
    {
        perror("fopen() failed");
        return 1;
    }

    while((c = getc(stdin)) != EOF)
    {
        if(0 == c) offset++;
        else
        {
            if(offset > threshold)
            {
                if(verbose) fprintf(stderr, "seek: offset CUR + %ld\n", offset);
                seeks++;
                fseek(fo, offset, SEEK_CUR);
                offset = 0;
            }
            else if(offset > 0) 
            {
                for(i=0; i<offset; i++) 
                    putc(0, fo);
                offset = 0;
            }

            putc(c, fo);
        }
    }

    if(offset > 0) 
    {
        if(verbose) fprintf(stderr, "seek: offset CUR + %ld\n", offset-1);
        seeks++;
        fseek(fo, offset-1, SEEK_CUR);
        putc(0, fo);
    }

    fclose(fo);

    if(verbose) fprintf(stderr, "total seeks: %lu\n", seeks);

    return 0;
} /* main() */

