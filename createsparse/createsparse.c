/******************************************************************************
 * createsparse.c
 * creates arbitrary sized sparse files
 *****************************************************************************/

#define _FILE_OFFSET_BITS 64
#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <errno.h>

static unsigned long long get_special_number(char *in)
{
    unsigned long long ret;
    char *unit;

    ret = strtoull(in, &unit, 10);
    if(*unit == 'k') { ret *= 1024; }
    else if(*unit == 'K') { ret *= 1000; }
    else if(*unit == 'm') { ret *= 1024*1024; }
    else if(*unit == 'M') { ret *= 1000*1000; }
    else if(*unit == 'g') { ret *= 1024*1024*1024; }
    else if(*unit == 'G') { ret *= 1000*1000*1000; }

    return ret;
} /* get_special_number() */


int main(int argc, char ** argv)
{
    size_t size;
    FILE *f;
    int err, i, ret=0;

    if(argc < 3)
    {
        fprintf(stderr, "Usage: createsparse <size[kKmMgG]> <filename> [filenames ...]\n\n");
        exit(1);
    }
        
    size = get_special_number(argv[1]);

    for(i=2; i < argc; i++)
    {
        f = fopen(argv[i], "w+");
        if(!f)
        {
            ret = 1;
            fprintf(stderr, "%s: fopen() failed: %s\n", argv[i], strerror(errno));
            continue;            
        }
        err = fseek(f, size-1, SEEK_SET);
        if(-1 == err)
        {
            ret = 1;
            fprintf(stderr, "%s: fseek() failed: %s\n", argv[i], strerror(errno));
            fclose(f);
            continue;            
        }
        err = putc(0, f);
        if(EOF == err)
        {
            ret = 1;
            fprintf(stderr, "%s: putc() failed: %s\n", argv[i], strerror(errno));
            fclose(f);
            continue;
        }
    } /* for i */

    return ret;
} /* main() */

